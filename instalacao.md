# Instalação Helm

Obs.: Certifique-se que tenha o Kubernetes instalado e que tenha um cluster do k8s em execução (pode ser minikube, Kind, bare metal ou cloud [GKE, AKS, EKS].  

## Para instalar o docker

* [Install docker](https://docs.docker.com/get-docker/)

## para instalar o kubectl

* [Install kubectl](https://kubernetes.io/docs/tasks/tools/)

## para instalar o Kind 

* [Install Kind](https://kind.sigs.k8s.io/docs/user/quick-start/)

## Para instalar o minikube

* [Install Minikube](https://minikube.sigs.k8s.io/docs/start/)

## Para instalar o HELM basta seguir os procedimentos nos links abaixo:

* [Install](https://helm.sh/docs/intro/install/)
