# Helm Para Ansiosos

Bem vindo a este repositório.  
O intuito é dar uma visão geral e fazer com que você consiga usar o HELM para gerenciar os pacotes de aplicações Kubernetes de forma rápida.  

Abaixo separei o material em 5 etapas, afinal é para ansiosos né!  

1. [Visão Geral](visao_geral.md)
2. [Instalação](instalacao.md)
3. [Pratica Imperativa](pratica_imperativa.md)
4. [Pratica Declarativa](pratica_declarativa.md)
5. [Referências](referencias.md)

Divirta-se, faça um fork e fique a vontade para compartilhar e fazer merge request se quiser.  
Um grande abraço!!  
