# Prática imperativa

Bora codar com o helm...  
Mas primeiro bora ver os comandos básicos...

## Criação do Cluster com o Kind (Kubernetes in Docker)
**ALERTA!: Não usar em Prod**  

Para instalar o kind tenha o docker e o docker compose em usa máquina, e siga os procedimentos abaixo:  

### Após instalar o kind crie um cluster conforme abaixo

Crie um cluster com o nome de seu desejo:   
```
kind create cluster --name my-cluster
```
Exemplo `kind create cluster --name anxiety-helm`

Para Ver o cluster use:  
```
kind get clusters
```

Para deletar os Clusters
```
kind delete cluster
```
Obs.: O parâmetro --name não é utilizado pois o Kind deleta o cluster que esta em contexto.  


## Comandos básicos

Para verificar a versão:  
```
helm version
```

Para listar as releases que tiveram o deploy executado:  
```
helm ls
```

## Inicializando o Repositório

```bash
# Repos do helm
helm repo add stable https://charts.helm.sh/stable
```
Obs.: Você pode inserir mais de um repositorio
Vamos instalar o repo da bitnami:  

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
```
Para visualizar os repositórios use o comando:  
```bash
helm repo ls
```

Agora que o Repositório está instalado e funcionando podemos pesquisar os charts por ele: 

```bash
helm search repo bitnami
```

Para instalarmos uma release usamos o comando:  
```bash
# Crie um namespace com o nome kafka
kubectl create namespace kafka
helm install meu-kafka  bitnami/kafka --namespace kafka
```

Aparecerá a seguinte mensagem:  

```
NAME: meu-kafka
LAST DEPLOYED: Wed May  5 20:14:18 2021
NAMESPACE: kafka
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
** Please be patient while the chart is being deployed **

Kafka can be accessed by consumers via port 9092 on the following DNS name from within your cluster:

    meu-kafka.kafka.svc.cluster.local

Each Kafka broker can be accessed by producers via port 9092 on the following DNS name(s) from within your cluster:

    meu-kafka-0.meu-kafka-headless.kafka.svc.cluster.local:9092

To create a pod that you can use as a Kafka client run the following commands:

    kubectl run meu-kafka-client --restart='Never' --image docker.io/bitnami/kafka:2.8.0-debian-10-r0 --namespace kafka --command -- sleep infinity
    kubectl exec --tty -i meu-kafka-client --namespace kafka -- bash

    PRODUCER:
        kafka-console-producer.sh \

            --broker-list meu-kafka-0.meu-kafka-headless.kafka.svc.cluster.local:9092 \
            --topic test

    CONSUMER:
        kafka-console-consumer.sh \
            --bootstrap-server meu-kafka.kafka.svc.cluster.local:9092 \
            --topic test \
            --from-beginning

```

Vamos listar nossos pods:  
```
kubectl get pods -n kafka
```
![](images/image5.png)


Abra janelas ou abas do terminal e use o seguinte comando em uma delas (lembre-se de estar com o contexto do kubernetes no kind) 

Vamos entrar no pod para usarmos com o seguinte comando:  
```
kubectl run meu-kafka-client --restart='Never' --image docker.io/bitnami/kafka:2.8.0-debian-10-r0 --namespace kafka --command -- sleep infinity
    kubectl exec --tty -i meu-kafka-client --namespace kafka -- bash
```
--- 

Ele entrará no pod, assim que entrar podemos enviar uma mensagem com o producer :  
```
kafka-console-producer.sh \
            --broker-list meu-kafka-0.meu-kafka-headless.kafka.svc.cluster.local:9092 \
            --topic palmeiras_nao_tem_mundial
```
![](images/image1.png)

![](images/image2.png)

Agora hora de entrar usando outro terminal e assim executar o producer e ver a mensagem:  
```
kafka-console-consumer.sh \
            --bootstrap-server meu-kafka.kafka.svc.cluster.local:9092 \
            --topic palmeiras_nao_tem_mundial\
            --from-beginning
```

Obs.: Você pode mudar o nome do tópico e enviar as mensagens a seu gosto... fique a vontade para brincar com os tópicos... o kafka é F0d@!!  



O mais legal é que quando você enviar uma mensagem ele aparece e a pega do outro lado no consumer .. 


![](images/image3.png)

Abaixo outro exemplo com o producer acima e o consumer abaixo:  

![](images/image4.png)

Para Finalizar limpe o cluster com o comando:  

```
helm delete nomedarelease
# depois 
kind delete  cluster --name anxiety-helm
```

## Resumo

Vimos neste exemplo como usar um helm chart padrão para poder subir um cluster com o kind e fazer o deploy de uma release do kafka.  
No próximo material veremos como criar um helm chart personalizado.  

